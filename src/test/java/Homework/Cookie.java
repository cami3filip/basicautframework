package Homework;
import Utils.OtherUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import tests.BaseTest;




public class Cookie extends BaseTest {
    WebDriver driver;

    @Test
    public void cookie(){
        WebElement button = driver.findElement(By.id("the_button"));
        button.click();
        Actions actions = new Actions(driver);
        driver.get("http://86.121.249.149:4999/stubs/cookie.html");
        OtherUtils.printCookie(driver.manage().getCookieNamed("myCookie"));
        org.openqa.selenium.Cookie cookie = new org.openqa.selenium.Cookie("myCookie", "abc");
        driver.manage().addCookie(cookie);
        OtherUtils.printCookies(driver);
        driver.manage().deleteCookieNamed("myCookie");
        driver.manage().deleteCookieNamed("abc");
        OtherUtils.printCookies(driver);
        driver.manage().deleteAllCookies();
        OtherUtils.printCookies(driver);


        button.click();



    }



}
