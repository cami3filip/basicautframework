package Homework;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PositiveRegistrationTest {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.ID, using = "inputFirstName")
    WebElement firstnameInput;
    @FindBy(xpath = "//*[@id=\"registration_form\"]/div[4]/div/input")
    WebElement lastnameInput;
    @FindBy(id = "inputEmail")
    WebElement emailInput;
    @FindBy (id = "inputUsername")
    WebElement usernameInput;
    @FindBy(id = "inputPassword")
    WebElement passwordInput;
    @FindBy(id = "inputPassword2")
    WebElement password2Input;




    public void register(String firstname, String lastname, String username, String email, String password, String confirmPassword) {
        firstnameInput.clear();
        firstnameInput.sendKeys(firstname);
        lastnameInput.clear();
        lastnameInput.sendKeys(lastname);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        password2Input.clear();
        password2Input.sendKeys(confirmPassword);



    }



    public void openRegisterPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/auth.html");
        driver.get(hostname + "/stubs/auth.html");
    }

}