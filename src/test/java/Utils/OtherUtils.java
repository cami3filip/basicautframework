package Utils;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Set;

public class OtherUtils {

    private static Object SeleniumUtils;

    public static void logInfoStatus(ExtentTest test, String message) {
        if (test != null)
            test.log(Status.INFO, message);
        System.out.println(message);
    }

    public static void printCookie(ExtentTest test, Cookie c) {
        logInfoStatus(test, "Cookie= " + c.getName() + " : " + c.getValue());
        if (c.getExpiry() != null)
            logInfoStatus(test, c.getExpiry().toString());
    }

    public static void printCookies(ExtentTest test, WebDriver driver) {
        Set<Cookie> cookies = driver.manage().getCookies();
        System.out.println("The number of cookies found are: " + cookies.size());
        for (Cookie c : cookies) {
            printCookie(test, c);
        }
    }

    public static void takeScreenShot(WebDriver driver) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File(SeleniumUtils + "\\screenshot-" + timestamp.getTime() + ".png");
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        } catch (IOException e) {
            System.out.println("File could not be saved");
        }
    }

    public static String sanitizeNullDbString(String dbResult) {
        if (dbResult == null) {
            return "";
        }
        return dbResult;
    }
}
